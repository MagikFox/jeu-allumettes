﻿using System;
using System.Threading;

namespace allum1
{
    class Program
    {
        static int main2()
        {
            Console.WriteLine("---- Jeu des allumettes ----");
            Console.Write("De combien est la base de ton jeu ? ");
            try
            {
                int Base = int.Parse(Console.ReadLine());
                if (Base % 2 == 0)
                {
                    Console.WriteLine("Tu peut mettre seulement des nombres impaire comme réponse !");
                    return main2();
                }
                else
                {
                    Console.Clear();
                    return Base;
                }
            }
            catch
            {
                Console.WriteLine("Tu ne peut pas laisser de vide !");
                return main2();
            }
        }
        static int Difficulty()
        {
            Console.WriteLine("Choisi la difficulté que tu veut");
            Console.WriteLine("(1) Easy");
            
            Console.WriteLine("(2) Hard");
            Console.WriteLine("(3) PVP");
            try
            {
                int Diff = int.Parse(Console.ReadLine());
                if (Diff <= 0)
                {
                    Console.WriteLine("Tu peut mettre que 1 ou 2 pour la difficulté");
                    return Difficulty();
                }
                else if (Diff > 4)
                {
                    Console.WriteLine("Tu ne peut pas mettre de chiffre plus grand que 3");
                    return Difficulty();
                }
                else
                {
                    return (Diff);
                }
            }
            catch
            {
                Console.WriteLine("Tu ne peut pas mettre da texte !");
                return Difficulty();
            }
        }

        static int calcTab(int Base)
        {
            int i = 1;
            int Nligne = 1; //nombre de ligne 
            while (Base != i)
            {
                i = i + 2;
                Nligne++;
            }
            return Nligne;
        }

        static int[] CreateBoard2(int Base, int Nligne)
        {
            int i = 0;
            int Nall = 1; //nombre allumettes 
            int[] Board = new int[Nligne];

            while (Nligne > i)
            {
                Board[i] = Nall;
                i++;
                Nall = Nall + 2;
            }
            return Board;
        }
        static int[] CreateRefBoard(int Base, int Nligne)
        {
            int i = 0;
            int Nall = 1; //nombre allumettes 
            int[] BoardRef = new int[Nligne];

            while (Nligne > i)
            {
                BoardRef[i] = Nall;
                i++;
                Nall = Nall + 2;
            }
            return BoardRef;
        }

        static int CreateBoard(int Base, int Nligne, int[] Board, int[] BoardRef)
        {
            int i = 0;
            int I = 1;
            while (Base + 2 != i)
            {
                Console.Write("-");
                i = i + 1;
            }
            i = 0;
            Console.WriteLine("");
            int L = 0;
            while (Base + 2 != I)
            {
                int I2 = 0;
                Console.Write("|");
                int space = Base - I;
                space = space / 2;
                int spaceTest = 0;
                int B = Board[L];
                int BR = BoardRef[L];


                while (space != spaceTest)
                {
                    Console.Write(" ");
                    spaceTest = spaceTest + 1;
                }
                spaceTest = 0;
                while (I2 != BoardRef[L])
                {
                    if (B != BR)
                    {
                        Console.Write(" ");
                        B++;
                        I2 = I2 + 1;
                    }
                    else if (B == BR)
                    {
                        Console.Write("I");
                        I2 = I2 + 1;
                    }
                }
                while (space != spaceTest)
                {
                    Console.Write(" ");
                    spaceTest = spaceTest + 1;
                }
                spaceTest = 0;
                Console.Write("|");
                Console.WriteLine(Board[L]);
                I = I + 2;
                L = L + 1;

            }
            while (Base + 2 != i)
            {
                Console.Write("-");
                i = i + 1;
            }
            Console.WriteLine("");
            Console.WriteLine("");
            return L;
        }
        static int CalcAllu(int Base, int L, int[] Board)
        {
            int i = 0;
            int i2 = 0;
            int nbmAllu = 0;
            int nbmAllu2 = 0;

            while (i2 != L)
            {
                nbmAllu = Board[i2];
                nbmAllu2 = nbmAllu + nbmAllu2;
                i2++;
                i++;
            }
            return (nbmAllu2);
        }
        static int Qligne(int L, int[] Board)
        {
            Console.WriteLine("Sur quelle ligne veut tu retirer une/des allumettes ? ");
            try
            {
                int Lremove_allumette = int.Parse(Console.ReadLine());
                if (Lremove_allumette <= 0)
                {
                    Console.WriteLine("tu ne peut pas mettre de chiffre négatif !");
                    return Qligne(L, Board);
                }
                else if (Lremove_allumette > L)
                {
                    Console.WriteLine("Tu ne peut pas mettre un nombre suppérieur au nombre de lignes !");
                    return Qligne(L, Board);
                }
                else
                {
                    if (Board[Lremove_allumette - 1] == 0)
                    {
                        Console.WriteLine("Tu n'a plus d'alumette dans cette ligne !");
                        return Qligne(L, Board);
                    }
                    else
                    {
                        return Lremove_allumette;
                    }
                }
            }
            catch
            {
                Console.WriteLine("Tu ne peut pas mettre de texte ici !!");
                return Qligne(L, Board);
            }

        }

        static int NbmAllumettes(int Lremove_allumette, int[] Board)
        {

            Console.Write("Combien d'allumette veut tu retirer ?");
            try
            {
                int NbaR = int.Parse(Console.ReadLine());
                if (NbaR <= 0)
                {
                    Console.WriteLine("Tu ne peut pas mettre de nombre en dessous de 1 !");
                    return NbmAllumettes(Lremove_allumette, Board);
                }
                else if (NbaR > Board[Lremove_allumette - 1])
                {
                    Console.WriteLine("Tu ne peut pas enlever plus d'allumette que ce que tu a !");
                    return NbmAllumettes(Lremove_allumette, Board);
                }
                else
                {
                    Board[Lremove_allumette - 1] = Board[Lremove_allumette - 1] - NbaR;  //retire le nombre d'allumette demandé 
                    return Board[Lremove_allumette - 1];
                }
            }
            catch
            {
                Console.WriteLine("Tu ne peut pas mettre de texte ici !");
                return NbmAllumettes(Lremove_allumette, Board);
            }
        }
        static void Loading()
        {
            int i = 0;
            while (i != 3)
            {
                i++;
                Console.Write(". ");
                Thread.Sleep(1000);
            }
            Console.WriteLine("");
            
        }

        static private void Main(string[] args)
        {

            string Player = "";
            int Base = main2();
            int Diff = Difficulty();
            int Nligne = calcTab(Base);
            int[] Board = CreateBoard2(Base, Nligne);
            int[] BoardRef = CreateRefBoard(Base, Nligne);
            int L = CreateBoard(Base, Nligne, Board, BoardRef);
            int nbmAllu2 = CalcAllu(Base, L, Board);
            int Lremove_allumette = Qligne(L, Board); ;
            Board[Lremove_allumette - 1] = NbmAllumettes(Lremove_allumette, Board);
            
            if (Diff != 3)
            {
                Player = "IA";
            }
            else if (Diff == 3)
            {
                Player = "Joueur 2";
            }
            CreateBoard(Base, Nligne, Board, BoardRef);
            while (nbmAllu2 != 0)
            {
                if (Diff != 3)
                {
                    if (nbmAllu2 != 0)
                    {
                        Console.WriteLine("C'est au tour de " + Player);

                        if (Player == "IA")
                        {
                            Loading();
                            IACall(Board, BoardRef, Nligne, Diff);
                            nbmAllu2 = CalcAllu(Base, L, Board);
                            Player = "Joueur";
                            CreateBoard(Base, Nligne, Board, BoardRef);
                        }
                    }
                    if (nbmAllu2 != 0)
                    {
                        if (Player == "Joueur")
                        {
                            Lremove_allumette = Qligne(L, Board);
                            Board[Lremove_allumette - 1] = NbmAllumettes(Lremove_allumette, Board);
                            nbmAllu2 = CalcAllu(Base, L, Board);

                            Player = "IA";
                            CreateBoard(Base, Nligne, Board, BoardRef);
                        }
                    }
                }
                else if (Diff == 3)
                {
                    Console.WriteLine("C'est au tour de " + Player);

                    if (nbmAllu2 != 0)
                    {
                        if (Player == "Joueur 2")
                        {
                            Lremove_allumette = Qligne(L, Board);
                            Board[Lremove_allumette - 1] = NbmAllumettes(Lremove_allumette, Board);
                            nbmAllu2 = CalcAllu(Base, L, Board);
                            Player = "Joueur 1";
                            CreateBoard(Base, Nligne, Board, BoardRef);
                        }
                    }
                    if(nbmAllu2 != 0)
                    {
                        if (Player == "Joueur 1")
                        {
                            Lremove_allumette = Qligne(L, Board);
                            Board[Lremove_allumette - 1] = NbmAllumettes(Lremove_allumette, Board);
                            nbmAllu2 = CalcAllu(Base, L, Board);
                            Player = "Joueur 2";
                            CreateBoard(Base, Nligne, Board, BoardRef);
                        }
                    }
                }

            }
            Fin(Player);


        }
        static void Fin(string Player)
        {
            Console.WriteLine("--------------------------------------------------------------");
            Console.WriteLine("La partie est terminé");
            Console.WriteLine("Bravo a " + Player + " pour avoir gagné la partie !");
            Console.WriteLine("--------------------------------------------------------------");
        }
        

        static void IACall(int[] Board, int[] BoardRef, int Nligne, int Diff)
        {
            if (Diff == 1)
            {
                IAEsay(Board, BoardRef, Nligne);
            }
            else if (Diff == 2)
            {
                IAHard(Board, BoardRef, Nligne);
            }
            else if (Diff == 3)
            {
                PVP(Board, BoardRef, Nligne);
            }
        }
        static void PVP(int[] Board, int[] BoardRef, int Nligne)
        {

        }
        static void IAEsay(int[] Board, int[] BoardRef, int Nligne)
        {
            bool IAPlay = true;
            Random Rand = new Random();
            int RemAllu;
            int TestLigne = 0;
            while (IAPlay == true)
            {

                TestLigne = Rand.Next(1, Nligne);
                if (Board[TestLigne] != 0)
                {
                    RemAllu = Rand.Next(1, Board[TestLigne]);
                    Board[TestLigne] = Board[TestLigne] - RemAllu;
                    Console.WriteLine("IA");
                    IAPlay = false;
                    
                }
                
            }

        }
        static void IANormal(int[] Board, int[] BoardRef, int Nligne)
        {

        }
        static void IAHard(int[] Board, int[] BoardRef, int Nligne)
        {
            int BoxOne = 0; // boite de 1
            int BoxTwo = 0; //  boite de 2
            int BoxFour = 0; // boite de 4
            int BoxRef = 0;
            int BoxBoardTest;
            int i = 1;
            int BoxNum;
            int[] SaveValueBox= new int[(Nligne*10)+11];
            while (i-1 != Nligne)
            {
                BoxNum = i * 10;
                BoxRef = Board[i-1] / 4; //test pour les boites de 4
                BoxFour = BoxRef + BoxFour;
                //SaveValueBox = new int[BoxNum+4]; //stock le nombre de brick de 4 pour la ligne 
                SaveValueBox[BoxNum+4] = BoxRef;
                BoxBoardTest = BoxRef * 4;
                if (BoxBoardTest != Board[i-1])
                {
                    
                    BoxRef = (Board[i-1] - (SaveValueBox[BoxNum + 4] * 4 )) / 2; // test pour les boites de 2 
                    BoxTwo = BoxRef + BoxTwo;
                    //SaveValueBox = new int[BoxNum + 2]; //stock le nombre de brick de 2 pour la ligne 
                    SaveValueBox[BoxNum+2] = BoxRef;
                    BoxBoardTest = BoxBoardTest + (BoxRef * 2);
                    if (BoxBoardTest != Board[i-1])
                    {
                        
                        BoxRef = (Board[i-1] - ((SaveValueBox[BoxNum + 2] * 2)+(SaveValueBox[BoxNum + 4] * 4))) / 1; // test pour les boites de 1
                        BoxOne = BoxRef + BoxOne;
                        //SaveValueBox = new int[BoxNum + 1]; //stock le nombre de brick de 1 pour la ligne 
                        SaveValueBox[BoxNum+1] = BoxRef;
                        BoxBoardTest = BoxBoardTest + BoxRef;
                    }
                }
                i = i + 1;
            }
            if (BoxFour % 2 != 0 && BoxTwo % 2 != 0 && BoxOne % 2 != 0)
            {
                BoxFTO(Board, BoardRef, SaveValueBox, Nligne);
            }
            else if (BoxFour % 2 != 0 && BoxTwo % 2 != 0)
            {
                BoxFT(Board, BoardRef, SaveValueBox, Nligne);
            }
            else if (BoxFour % 2 != 0 && BoxOne % 2 != 0)
            {
                BoxFO(Board, BoardRef, SaveValueBox, Nligne);
            }
            else if (BoxTwo % 2 != 0 && BoxOne % 2 != 0)
            {
                BoxOT(Board, BoardRef, SaveValueBox, Nligne);
            }
            else if (BoxFour % 2 != 0)
            {
                BoxF(Board, BoardRef, SaveValueBox, Nligne);
            }
            else if (BoxTwo % 2 != 0)
            {
                BoxT(Board, BoardRef, SaveValueBox, Nligne);
            }
            else if (BoxOne%2 != 0)
            {
                BoxO(Board, BoardRef, SaveValueBox, Nligne);
            }
            else
            {
                bool IAPlay = true;
                Random Rand = new Random();
                int RemAllu;
                int TestLigne = 0;
                while (IAPlay == true)
                {
                    TestLigne = Rand.Next(1, Nligne);
                    if (Board[TestLigne] != 0)
                    {
                        RemAllu = Rand.Next(1, Board[TestLigne]);
                        Board[TestLigne] = Board[TestLigne] - RemAllu;
                        IAPlay = false;
                    }

                }

            }
        }

        static void BoxF(int[] Board, int[] BoardRef, int[] SaveValueBox, int Nligne)
        {
            bool Stay = true; 
            Random Rand = new Random();
            int i = 0;
            int testF = 0;
            while(Stay == true)
            {
                if(SaveValueBox[testF] != 0)
                {
                    Stay = false;
                }
                else
                {
                    i = Rand.Next(1, Nligne);
                    testF = (i * 10) + 4;
                    Console.WriteLine("F");
                }
            }
            Console.WriteLine(i);
            Board[i] = Board[i] - (SaveValueBox[testF]*4);
        }
        static void BoxT(int[] Board, int[] BoardRef, int[] SaveValueBox, int Nligne)
        {
            bool Stay = true; 
            Random Rand = new Random();
            int i = 0;
            int testT = 0;
            while (Stay == true)
            {
                if(SaveValueBox[testT] != 0)
                {
                    Stay = false;
                }
                else
                {
                    i = Rand.Next(1, Nligne);
                    testT = (i * 10) + 2;
                    Console.WriteLine("T");
                }
            }
            Console.WriteLine(i);
            Board[i] = Board[i] - (SaveValueBox[testT]*2);
        }
        static void BoxO(int[] Board, int[] BoardRef, int[] SaveValueBox, int Nligne)
        {
            bool Stay = true; 
            Random Rand = new Random();
            int i = 0;
            int testO = 0;
            while (Stay == true)
            {
                if(SaveValueBox[testO] != 0)
                {
                    Stay = false;
                }
                else
                {
                    i = Rand.Next(1, Nligne);
                    testO = (i * 10) + 1;
                    Console.WriteLine("O");
                }
                
            }
            Console.WriteLine(i);
            Board[i] = Board[i] - SaveValueBox[testO];
        }
        static void BoxOT(int[] Board, int[] BoardRef, int[] SaveValueBox, int Nligne)
        {
            bool Stay = true; 
            int i = 0;
            int testO = 0;
            int testT = 0;
            while (Stay == true)
            {
                if(SaveValueBox[testO] != 0 && SaveValueBox[testT] != 0)
                {
                    Stay = false;
                }
                else
                {
                    i++;
                    testO = (i * 10) + 1;
                    testT = (i * 10) + 2;
                    Console.WriteLine("OT");
                }
            }
            Console.WriteLine(i);
            Board[i] = Board[i] - (SaveValueBox[testO] + (SaveValueBox[testT]*2));
        }
        static void BoxFO(int[] Board, int[] BoardRef, int[] SaveValueBox, int Nligne)
        {
            bool Stay = true;
            int i = 0;
            int testF = 0;
            int testO = 0;
            while (Stay == true)
            {
                if(SaveValueBox[testF] != 0 && SaveValueBox[testO] != 0)
                {
                    Stay = false;
                }
                else
                {
                    i++;
                    testF = (i * 10) + 4;
                    testO = (i * 10) + 1;
                    Console.WriteLine("FO");
                }
                
            }
            Console.WriteLine(i);
            Board[i] = Board[i] - ((SaveValueBox[testF]*4) + SaveValueBox[testO]);
        }
        static void BoxFT(int[] Board, int[] BoardRef, int[] SaveValueBox, int Nligne)
        {
            bool Stay = true;
            int i = 0;
            int testF = 0;
            int testT = 0;
            while (Stay == true)
            {
                if(SaveValueBox[testF] != 0 && SaveValueBox[testT] != 0)
                {
                    Stay = false;
                }
                else
                {
                    i++;
                    testF = (i * 10) + 4;
                    testT = (i * 10) + 2;
                    Console.WriteLine("FT");
                }
            }
            Console.WriteLine(i);
            Board[i] = Board[i] - ((SaveValueBox[testF]*4) + (SaveValueBox[testT]*2));
        }
        static void BoxFTO(int[] Board, int[] BoardRef, int[] SaveValueBox, int Nligne)
        {
            bool Stay = true;
            int i = 1;
            int testF = 0;
            int testT = 0;
            int testO = 0;
            testF = (i * 10) + 4;
            testT = (i * 10) + 2;
            testO = (i * 10) + 1;
            while (Stay == true)
            {
                if(SaveValueBox[testF] != 0 && SaveValueBox[testT] != 0 && SaveValueBox[testO] != 0)
                {
                    Stay = false;
                }
                else 
                {
                    i++;
                    testF = (i * 10) + 4;
                    testT = (i * 10) + 2;
                    testO = (i * 10) + 1;
                    Console.WriteLine("FTO");
                }
            }
            Console.WriteLine(i-1);
            Board[i-1] = Board[i-1] - ((SaveValueBox[testF]*4)+ (SaveValueBox[testT]*2)+ SaveValueBox[testO]);
        }
    }    
}